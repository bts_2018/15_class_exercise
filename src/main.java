import java.io.File;
import java.util.Scanner;

public class main {

	public static void main(String[] args) {
String[] line;
		
		try {
			   File file = new File("src/import.txt");
			   Scanner scanner = new Scanner(file);
			   	  
			   while (scanner.hasNext()) {
				   line = scanner.next().split(";");
				   Person x = new Person(line[0], line[1], line[2]);
				   if (x.isAdult()== false) {
					   System.out.println(x.name +  " " + x.surname + " is not adult");
				   }
				 
			   }
			   
			   scanner.close();
			   
		} catch (Exception e) {
			 System.out.println("ERROR: " + e.getMessage());
		} 
		

	}

}
